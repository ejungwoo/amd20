// Root header
#include <TFile.h>
#include <TTree.h>
#include <TMath.h>
#include <TCanvas.h>
#include <TH2D.h>

// c/c++ header
#include <fstream>
#include <iostream>


double CalculatePT(double pX, double pY);
double CalculatePhi(double pX, double pY);
double CalculateTheta(double pZ, double pT);
double CalculateRapidity(double e, double pZ);
double CalculatePBeamLab(double sNN, double m);
double CalculateTotalEnergy(double p, double m);

double CalculateThetaLab(double theta, double p, double m, double gamma, double beta);
double CalculateEnergyLT(double e, double pZ, double gamma, double beta);
double CalculateMomentumLT(double e, double pZ, double gamma, double beta);

int main(int argc, char **argv)
{
  //general info
  const int nline = 124715;

  int Z, N, eventID, dummyInt;
  double pX, pY, pZ, dummyDouble;

  int eventID_before=1;
  int iteration=0;

  fstream openFile("data/rawData.dat");

  TFile *writeFile = new TFile("data/AMD.root","recreate");
  TTree *rawTree = new TTree("raw", "raw tree");
         rawTree -> Branch("Z",&Z,"Z/I");
         rawTree -> Branch("N",&N,"N/I");
         rawTree -> Branch("pX",&pX,"pX/D");
         rawTree -> Branch("pY",&pY,"pY/D");
         rawTree -> Branch("pZ",&pZ,"pZ/D");
         rawTree -> Branch("eventID",&eventID,"eventID/I");

  for(int i=0; i<nline; i++)
  {
    eventID_before = eventID-iteration*201;
    openFile >> Z >> N >> pX >> pY >> pZ >> dummyInt >> dummyInt >> dummyDouble >> dummyDouble >> eventID >> dummyInt;
    if(eventID_before>eventID) iteration++;
    eventID+=iteration*201;
    rawTree -> Fill();
  }
  openFile.close();

  int    A; //mass number 
  double pT, theta, phi, eKin, eTot, eRest, rapidity;
  double m0; // rest mass

  //general
  double mProton  = 938.272; // MeV/c2
  double mNeutron = 939.565; // MeV/c2
  double mNucleon = (mProton + mNeutron)*0.5; // MeV/c2

  //Beam(projectile 132Sn)
  int    ZBeam = 50; // pronton number of Beam
  int    NBeam = 82; // pronton number of Beam
  int    ABeam = ZBeam + NBeam;  // mass number of beam
  double mBeam = mProton*ZBeam + mNeutron*NBeam; // pronton number of Beam (MeV/c2)

  //Target(124Sn)
  int    ZTarget = 50;
  int    NTarget = 74;
  int    ATarget = ZTarget + NTarget;
  double mTarget = mProton*ZTarget + mNeutron*NTarget;

  //Beam - Lab
  double eKinBeam_lab = 20; // MeV
  double eBeam_lab    = eKinBeam_lab + mNucleon; // MeV
  double pBeam_lab    = TMath::Sqrt(eBeam_lab*eBeam_lab - mNucleon*mNucleon); // MeV/c
  double rapidityBeam_lab = CalculateRapidity(eBeam_lab, pBeam_lab);
  //Center of mass energy
  double sNN      = TMath::Power(eBeam_lab + mNucleon,2) - TMath::Power(pBeam_lab,2);
  double sqrt_sNN = sqrt(sNN);
  double cmEnergy = sqrt_sNN - 2*mNucleon;
  //Beam - CM
  double eBeam_cm     = sqrt_sNN/2;
  double eKinBeam_cm  = eBeam_cm - mNucleon;
  double pBeam_cm     = TMath::Sqrt(eBeam_cm*eBeam_cm - mNucleon*mNucleon); //
  double rapidityBeam_cm  = CalculateRapidity(eBeam_cm, pBeam_cm); //rapidity
  //General 
  double beta      = -pBeam_lab/(eBeam_lab+mNucleon);
  double gamma     = 1/sqrt(1-beta*beta);


  int entries = rawTree -> GetEntries();

  double pZLab;

  eventID=1;
  eventID_before=1;
  TTree *tree;

  for(int iEntry=0; iEntry<entries; iEntry++)
  {
    eventID_before=eventID;
    rawTree -> GetEntry(iEntry);

    if(eventID_before<eventID || iEntry==0)
    {
      if(eventID!=1)
      {
        std::cout << eventID-1 << std::endl;
        writeFile -> cd();
        tree -> Write();
      }

      TTree *labTree = new TTree(Form("event_%d",eventID), "");
      labTree -> Branch("Z",&Z,"Z/I");
      labTree -> Branch("N",&N,"N/I");
      labTree -> Branch("pX",&pX,"pX/D");
      labTree -> Branch("pY",&pY,"pY/D");
      labTree -> Branch("pZ",&pZLab,"pZ/D");

      tree = labTree;
    }

    A  = Z+N;
    pX = pX*A;                                          //x-direction momentum of fragment 
    pY = pY*A;                                          //y-direction momentum of fragment 
    pZ = pZ*A;                                          //z-direction momentum of fragment 
    m0 = mProton*Z + mNeutron*N;                        //rest mass
    eTot = TMath::Sqrt(pX*pX + pY*pY + pZ*pZ + m0*m0);  //total energy 
    pZLab = CalculateMomentumLT(eTot, pZ, gamma, beta);

    tree -> Fill();
  }

  std::cout << eventID << std::endl;
  writeFile -> cd();
  tree -> Write();

  delete rawTree;
  delete writeFile;

  return 0;
}

double CalculatePT(double pX, double pY)    { return TMath::Sqrt(pX*pX + pY*pY); }
double CalculatePhi(double pX, double pY)   
{ 
  double phi;

  if(pX>=0&&pY>=0) phi = TMath::ATan(pY/pX);
  else if(pX>=0&&pY<0) phi = 2*TMath::Pi()+TMath::ATan(pY/pX);
  else phi = TMath::Pi()+TMath::ATan(pY/pX);

  return phi;
}
double CalculateTheta(double pZ, double pT) { return TMath::ATan(pT/pZ); }
double CalculateRapidity(double e, double pZ)   { return TMath::Log((e+pZ)/(e-pZ))/2; }
double CalculatePBeamLab(double sNN, double m)  { return m*sqrt(TMath::Power((sNN/(2*m*m)-1),2)-1); }
double CalculateTotalEnergy(double p, double m) { return sqrt(p*p + m*m); }

double CalculateThetaLab(double theta, double p, double m, double gamma, double beta)
{ return TMath::ATan((p*TMath::Sin(theta))/(gamma*(p*TMath::Cos(theta)+beta*m))); }
double CalculateEnergyLT(double e, double pZ, double gamma, double beta)
{ return gamma*e-gamma*beta*pZ; }
double CalculateMomentumLT(double e, double pZ, double gamma, double beta)
{ return -gamma*beta*e+gamma*pZ; }

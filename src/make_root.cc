// Root header
#include <TFile.h>
#include <TTree.h>
#include <TMath.h>
#include <TCanvas.h>
#include <TH2D.h>

// c/c++ header
#include <fstream>
#include <iostream>


double CalculatePT(double pX, double pY);
double CalculatePhi(double pX, double pY);
double CalculateTheta(double pZ, double pT);
double CalculateRapidity(double e, double pZ);
double CalculatePBeamLab(double sNN, double m);
double CalculateTotalEnergy(double p, double m);

double CalculateThetaLab(double theta, double p, double m, double gamma, double beta);
double CalculateEnergyLT(double e, double pZ, double gamma, double beta);
double CalculateMomentumLT(double e, double pZ, double gamma, double beta);

int main(int argc, char **argv)
{
  //general info
  const int nline = 124715;

  std::cout << std::endl;
  int Z, N, eventID, dummyInt;
  double pX, pY, pZ, dummyDouble;

  fstream openFile("data/rawData.dat");

  TFile *writeFile = new TFile("data/amdData.root","recreate");
  TTree *rawTree = new TTree("raw", "raw tree");
         rawTree -> Branch("Z",&Z,"Z/I");
         rawTree -> Branch("N",&N,"N/I");
         rawTree -> Branch("pX",&pX,"pX/D");
         rawTree -> Branch("pY",&pY,"pY/D");
         rawTree -> Branch("pZ",&pZ,"pZ/D");

  for(int i=0; i<nline; i++)
  {
    openFile >> Z >> N >> pX >> pY >> pZ >> dummyInt >> dummyInt >> dummyDouble >> dummyDouble >> eventID >> dummyInt;
    rawTree -> Fill();
  }
  openFile.close();

  writeFile -> cd();
  rawTree -> Write();

  int    A; //mass number 
  double pT, theta, phi, eKin, eTot, eRest, rapidity;
  double m0; // rest mass

  //general
  double mProton  = 938.272; // MeV/c2
  double mNeutron = 939.565; // MeV/c2
  double mNucleon = (mProton + mNeutron)*0.5; // MeV/c2

  //Beam(projectile 132Sn)
  int    ZBeam = 50; // pronton number of Beam
  int    NBeam = 82; // pronton number of Beam
  int    ABeam = ZBeam + NBeam;  // mass number of beam
  double mBeam = mProton*ZBeam + mNeutron*NBeam; // pronton number of Beam (MeV/c2)

  //Target(124Sn)
  int    ZTarget = 50;
  int    NTarget = 74;
  int    ATarget = ZTarget + NTarget;
  double mTarget = mProton*ZTarget + mNeutron*NTarget;

  //Beam - Lab
  double eKinBeam_lab = 20; // MeV
  double eBeam_lab    = eKinBeam_lab + mNucleon; // MeV
  double pBeam_lab    = TMath::Sqrt(eBeam_lab*eBeam_lab - mNucleon*mNucleon); // MeV/c
  double rapidityBeam_lab = CalculateRapidity(eBeam_lab, pBeam_lab);
  //Center of mass energy
  double sNN      = TMath::Power(eBeam_lab + mNucleon,2) - TMath::Power(pBeam_lab,2);
  double sqrt_sNN = sqrt(sNN);
  double cmEnergy = sqrt_sNN - 2*mNucleon;
  //Beam - CM
  double eBeam_cm     = sqrt_sNN/2;
  double eKinBeam_cm  = eBeam_cm - mNucleon;
  double pBeam_cm     = TMath::Sqrt(eBeam_cm*eBeam_cm - mNucleon*mNucleon); //
  double rapidityBeam_cm  = CalculateRapidity(eBeam_cm, pBeam_cm); //rapidity
  //General 
  double beta      = -pBeam_lab/(eBeam_lab+mNucleon);
  double gamma     = 1/sqrt(1-beta*beta);

  std::cout << "* General *" << std::endl;
  std::cout << "sqrt_sNN : " << sqrt_sNN << std::endl;
  std::cout << "cm energy: " << cmEnergy << std::endl;
  std::cout << "gamma    : " << gamma << std::endl;
  std::cout << "beta     : " << beta << std::endl;
  std::cout << std::endl;
  std::cout << "* LAB *" << std::endl;
  std::cout << "energy_beam/u   : " << eBeam_lab << std::endl;
  std::cout << "kinetic energy/u: " << eKinBeam_lab << std::endl;
  std::cout << "momentum_beam/u : " << pBeam_lab << std::endl;
  std::cout << "rapidity        : " << rapidityBeam_lab << std::endl;
  std::cout << std::endl;
  std::cout << "* CM *" << std::endl;
  std::cout << "energy_beam/u   : " << eBeam_cm << std::endl;
  std::cout << "kinetic energy/u: " << eKinBeam_cm << std::endl;
  std::cout << "momentum_beam/u : " << pBeam_cm << std::endl;
  std::cout << "rapidity        : " << rapidityBeam_cm << std::endl;
  std::cout << std::endl;
  std::cout << "y shift: " << TMath::Log(sqrt((1-beta)/(1+beta)) ) << std::endl;
  std::cout << std::endl;

  //-------------------------------------------------------------------------CM Frame

  TTree *cmTree = new TTree("cm", "modified cm frame");
         cmTree -> Branch("A",&A,"A/I");
         cmTree -> Branch("Z",&Z,"Z/I");
         cmTree -> Branch("N",&N,"N/I");
         cmTree -> Branch("pX",&pX,"pX/D");
         cmTree -> Branch("pY",&pY,"pY/D");
         cmTree -> Branch("pZ",&pZ,"pZ/D");
         cmTree -> Branch("pT",&pT,"pT/D");
         cmTree -> Branch("theta",&theta,"theta/D");
         cmTree -> Branch("phi",&phi,"phi/D");
         cmTree -> Branch("eTot",&eTot,"eTot/D");
         cmTree -> Branch("eKin",&eKin,"eKin/D");
         cmTree -> Branch("rapidity",&rapidity,"rapidity/D");
         cmTree -> Branch("eventID",&eventID,"eventID/I");

  int entries = rawTree -> GetEntries();

  for(int iEntry=0; iEntry<entries; iEntry++)
  {
    rawTree -> GetEntry(iEntry);

    A  = Z+N; //mass number

    pX = pX*A;                 //x-direction momentum of fragment 
    pY = pY*A;                 //y-direction momentum of fragment 
    pZ = pZ*A;                 //z-direction momentum of fragment 
    pT = CalculatePT(pX, pY); //transverse momentum of fragment

    theta = CalculateTheta(pZ, pT); //theta
    if(theta<0) theta = theta+TMath::Pi();

    phi = CalculatePhi(pX, pY);   //phi

    m0       = mProton*Z + mNeutron*N;                     //rest mass
    eTot     = TMath::Sqrt(pX*pX + pY*pY + pZ*pZ + m0*m0); //total energy 
    eRest    = m0;
    eKin     = eTot - eRest;                               //kinetic energy
    rapidity = CalculateRapidity(eTot, pZ)/rapidityBeam_cm;   //normalized rapditiy 

    cmTree -> Fill();
  }

  writeFile -> cd();
  cmTree -> Write();

  //-------------------------------------------------------------------------LAB Frame

  double thetaLab, rapidityLab, pZLab;
  double pTot;
  //double phiLab; // phi in lab system is equal to phi in cm system

  //TCanvas *cvs = new TCanvas("cvs","",800,800);
  //TH2D *hist = new TH2D("hist","",100,0,3.2,100,0,3.2);

  TTree *labTree = new TTree("lab", "modified lab frame");
         labTree -> Branch("A",&A,"A/I");
         labTree -> Branch("Z",&Z,"Z/I");
         labTree -> Branch("N",&N,"N/I");
         labTree -> Branch("pT",&pT,"pT/D");
         labTree -> Branch("pZ",&pZLab,"pZ/D");
         labTree -> Branch("pTot",&pTot,"pTot/D");
         labTree -> Branch("theta",&thetaLab,"theta/D");
         labTree -> Branch("phi",&phi,"phi/D");
         labTree -> Branch("eTot",&eTot,"eTot/D");
         labTree -> Branch("eKin",&eKin,"eKin/D");
         labTree -> Branch("rapidity",&rapidityLab,"rapidity/D");

  for(int iEntry=0; iEntry<entries; iEntry++)
  {
    cmTree -> GetEntry(iEntry);

    pZLab = CalculateMomentumLT(eTot, pZ, gamma, beta);
    eTot  = CalculateEnergyLT(eTot, pZ, gamma, beta);
    eKin  = eTot - (mProton*Z + mNeutron*N); 
    rapidityLab = CalculateRapidity(eTot,pZLab)/rapidityBeam_lab;
    pTot  = sqrt(pT*pT + pZLab*pZLab);
    thetaLab = CalculateTheta(pZLab,pT);
    if(thetaLab<0) thetaLab = thetaLab+TMath::Pi();

    //thetaLab = CalculateThetaLab(theta, pTot, mNucleon*A, gamma, beta);
    //if(thetaLab<0) thetaLab = thetaLab+TMath::Pi();
    //hist -> Fill(theta, thetaLab);

    labTree -> Fill();
  }
  //cvs -> cd();
  //hist -> Draw();

  writeFile -> cd();
    labTree -> Write();
    //cvs -> Write();

  delete rawTree;
  delete cmTree;
  delete labTree;
  delete writeFile;

  return 0;
}

//함수들의 정의
double CalculatePT(double pX, double pY)    { return TMath::Sqrt(pX*pX + pY*pY); }
double CalculatePhi(double pX, double pY)   
{ 
  double phi;

  if(pX>=0&&pY>=0) phi = TMath::ATan(pY/pX);
  else if(pX>=0&&pY<0) phi = 2*TMath::Pi()+TMath::ATan(pY/pX);
  else phi = TMath::Pi()+TMath::ATan(pY/pX);

  return phi;
}
double CalculateTheta(double pZ, double pT) { return TMath::ATan(pT/pZ); }
double CalculateRapidity(double e, double pZ)   { return TMath::Log((e+pZ)/(e-pZ))/2; }
double CalculatePBeamLab(double sNN, double m)  { return m*sqrt(TMath::Power((sNN/(2*m*m)-1),2)-1); }
double CalculateTotalEnergy(double p, double m) { return sqrt(p*p + m*m); }

double CalculateThetaLab(double theta, double p, double m, double gamma, double beta)
{ return TMath::ATan((p*TMath::Sin(theta))/(gamma*(p*TMath::Cos(theta)+beta*m))); }
double CalculateEnergyLT(double e, double pZ, double gamma, double beta)
{ return gamma*e-gamma*beta*pZ; }
double CalculateMomentumLT(double e, double pZ, double gamma, double beta)
{ return -gamma*beta*e+gamma*pZ; }

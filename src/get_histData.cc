// Root header
#include <TCanvas.h>
#include <TStyle.h>
#include <TFile.h>
#include <TMath.h>
#include <TTree.h>
#include <TCut.h>
#include <TPad.h>
#include <TH1D.h>
#include <TF1.h>

// c/c++ header
#include <string>

double GetYMean(TH1D *hist, int nbinsx);

void ZAnalysis(TTree *labTree, TFile *histData); //Fixed
void ThetaAnalysis(TTree *labTree, TFile *histData); //Fixed
void PhiAnalysis(TTree *labTree, TFile *histData);
void EKinAnalysis(TTree *labTree, TFile *histData); //Fixed
void PTAnalysis(TTree *labTree, TFile *histData);
void RapidityAnalysis(TTree *labTree, TFile *histData);

void ThetaDivision(TTree *labTree, TFile *histData); //Fixed
void ThetaDivision5(TTree *labTree, TFile *histData); //Fixed
void ThetaDeg(TTree *labTree, TFile *histData); //Fixed
void PhiDeg(TTree *labTree, TFile *histData); //Fixed

const int nEvent = 2010;

int main()
{
  std::cout << std::endl << std::endl;
  std::cout << "Drawing histograms..." << std::endl;

  gStyle -> SetOptStat(0);
  gStyle -> SetTitleYOffset(1.5);
  gStyle -> SetTitleXOffset(1.1);
  gStyle -> SetTitleSize(0.05,"xy");
  gStyle -> SetPadLeftMargin(0.16);
  gStyle -> SetPadRightMargin(0.05);
  gStyle -> SetPadTopMargin(0.13);
  gStyle -> SetPadBottomMargin(0.12);

  TFile *amdData = new TFile("data/amdData.root","read");
  TTree *labTree = (TTree*) amdData -> Get("lab");

  TFile *histData = new TFile("data/histData.root","recreate");

  ZAnalysis(labTree, histData);
  ThetaAnalysis(labTree, histData);
  PhiAnalysis(labTree, histData);
  EKinAnalysis(labTree, histData);
  PTAnalysis(labTree, histData);
  RapidityAnalysis(labTree, histData);

  ThetaDivision(labTree, histData);
  ThetaDivision5(labTree, histData);
  ThetaDeg(labTree, histData);
  PhiDeg(labTree, histData);

  return 0;
}

void ThetaDivision(TTree *labTree, TFile *histData)
{
  const int nbinsx = 8;
  double xbins[nbinsx+1] = {0,5.5,10,15.5,22,30,40,60,120};
  double weight = ((double)(1))/nEvent;

  TCanvas *cvs = new TCanvas("theta_division","",800,800);
  TH1D *hist = new TH1D("hist_division",";#theta (degrees);N(#theta' #leq #theta < #theta'+#Delta#theta')",nbinsx, xbins);
        hist -> GetXaxis() -> CenterTitle();
        hist -> GetYaxis() -> CenterTitle();
  cvs -> cd();
  labTree -> SetWeight(weight);
  labTree -> Draw("theta*180/TMath::Pi()>>hist_division","Z!=0");

  histData -> cd();
  cvs -> Write();

  std::cout << std::endl << "* theta division" << std::endl;
  std::cout << "normal : " << hist -> Integral("width") << std::endl;
  std::cout << std::endl;

  cvs -> SaveAs("doc/figures/theta_diffBin.pdf");

  delete cvs;
  delete hist;
}

void ThetaDivision5(TTree *labTree, TFile *histData)
{
  double xlow   = 0;
  double xup    = 180;
  double sizex  = 5;
  double nbinsx = (xup - xlow)/sizex;
  double weight = ((double)(1))/nEvent;

  TCanvas *cvs = new TCanvas("theta_division5","",800,800);
  TH1D *hist = new TH1D("hist_division5",";#theta (degrees);N(#theta' #leq #theta < #theta'+#Delta#theta')",nbinsx, xlow, xup);
        hist -> GetXaxis() -> CenterTitle();
        hist -> GetYaxis() -> CenterTitle();
  cvs -> cd();
  labTree -> SetWeight(weight);
  labTree -> Draw("theta*180/TMath::Pi()>>hist_division","Z!=0");

  histData -> cd();
  cvs -> Write();

  std::cout << std::endl << "* theta division5" << std::endl;
  std::cout << "normal : " << hist -> Integral("width") << std::endl;
  std::cout << std::endl;

  cvs -> SaveAs("doc/figures/theta_diffBin5.pdf");

  delete cvs;
  delete hist;
}

void ZAnalysis(TTree *labTree, TFile *histData)
{
  int    nbinsx = 74;
  double xlow   = 0;
  double xup    = 74;
  double weight = (nbinsx/(xup - xlow))/nEvent;

  TCanvas *cvs = new TCanvas("Z","",800,800);
           cvs -> SetLogy(1);
  TH1D *hist = new TH1D("hist_Z","Z (^{132}Sn+^{124}Sn @ 20 MeV/u (AMD));Z;(1/N_{event})(dN/dZ)",nbinsx, xlow, xup);

        hist -> GetXaxis() -> CenterTitle();
        hist -> GetYaxis() -> CenterTitle();

  labTree -> SetWeight(weight);
  labTree -> Draw("Z>>hist_Z");
  cvs -> cd();
  hist -> Draw();
  histData -> cd();
  cvs -> Write();

  std::cout << std::endl << "* Z" << std::endl;
  std::cout << "y-av-0 : " << GetYMean(hist,nbinsx) << std::endl;
  std::cout << "normal : " << hist -> Integral("width") << std::endl;
  std::cout << std::endl;

  cvs -> SaveAs("doc/figures/Z.pdf");

  delete cvs;
  delete hist;
}

void PhiDeg(TTree *labTree, TFile *histData)
{
  int    nbinsx = 60;
  double xlow   = 0;
  double xup    = 360;
  double weight = (nbinsx/(xup - xlow))/nEvent;

  TCanvas *cvs = new TCanvas("phi_deg","",800,800);
  TCanvas *cvsCharged = new TCanvas("phi_charged_deg","",800,800);
  TCanvas *cvsNeutral = new TCanvas("phi_neutral_deg","",800,800);

  TH1D *hist = new TH1D("hist_phi_deg","#phi (^{132}Sn+^{124}Sn @ 20 MeV/u (AMD));#phi (degrees);(1/N_{event})(dN/d#phi)",nbinsx, xlow, xup);
  hist -> SetMinimum(0);
  TH1D *histCharged = new TH1D("hist_phi_charged_deg","#phi_{charged} (^{132}Sn+^{124}Sn @ 20 MeV/u (AMD));#phi (degrees);(1/N_{event})(dN/d#phi)",nbinsx, xlow, xup);
  histCharged -> SetMinimum(0);
  TH1D *histNeutral = new TH1D("hist_phi_neutral_deg","#phi_{neutral} (^{132}Sn+^{124}Sn @ 20 MeV/u (AMD));#phi (degrees);(1/N_{event})(dN/d#phi)",nbinsx, xlow, xup);
  histNeutral -> SetMinimum(0);

        hist -> GetXaxis() -> CenterTitle();
        hist -> GetYaxis() -> CenterTitle();
        histCharged -> GetXaxis() -> CenterTitle();
        histCharged -> GetYaxis() -> CenterTitle();
        histNeutral -> GetXaxis() -> CenterTitle();
        histNeutral -> GetYaxis() -> CenterTitle();

  labTree -> SetWeight(weight);
  cvs -> cd();
  labTree -> Draw("phi*180/TMath::Pi()>>hist_phi_deg");
  cvsCharged -> cd();
  labTree -> Draw("phi*180/TMath::Pi()>>hist_phi_charged_deg","Z!=0");
  cvsNeutral -> cd();
  labTree -> Draw("phi*180/TMath::Pi()>>hist_phi_neutral_deg","Z==0");

  std::cout << std::endl << "* phi in degrees" << std::endl;
  std::cout << "y-av-0 : " << GetYMean(hist,nbinsx) << std::endl;
  std::cout << "y-av-n : " << GetYMean(histNeutral,nbinsx) << std::endl;
  std::cout << "y-av-c : " << GetYMean(histCharged,nbinsx) << std::endl;
  std::cout << "normal : " << hist -> Integral("width") << std::endl;
  std::cout << "charged: " << histCharged -> Integral("width") << std::endl;
  std::cout << "neutral: " << histNeutral -> Integral("width") << std::endl;
  std::cout << std::endl;

  histData -> cd();
    cvs -> Write();
    cvsCharged -> Write();
    cvsNeutral -> Write();

  cvs -> SaveAs("doc/figures/phi_deg.pdf");
  cvsCharged -> SaveAs("doc/figures/phi_charged_deg.pdf");
  cvsNeutral -> SaveAs("doc/figures/phi_neutral_deg.pdf");

  delete cvs;
  delete cvsCharged;
  delete cvsNeutral;

  delete hist;
  delete histCharged;
  delete histNeutral;
}

void ThetaDeg(TTree *labTree, TFile *histData)
{
  int    nbinsx = 60;
  double xlow   = 0;
  double xup    = 180;
  double weight = (nbinsx/(xup - xlow))/nEvent;

  TCanvas *cvs = new TCanvas("theta_deg","",800,800);
  TCanvas *cvsCharged = new TCanvas("theta_charged_deg","",800,800);
  TCanvas *cvsNeutral = new TCanvas("theta_neutral_deg","",800,800);

  TH1D *hist = new TH1D("hist_theta_deg","#theta (^{132}Sn+^{124}Sn @ 20 MeV/u (AMD));#theta (degrees);(1/N_{event})(dN/d#theta)",nbinsx, xlow, xup);
  TH1D *histCharged = new TH1D("hist_theta_charged_deg","#theta_{charged} (^{132}Sn+^{124}Sn @ 20 MeV/u (AMD));#theta (degrees);(1/N_{event})(dN/d#theta)",nbinsx, xlow, xup);
  TH1D *histNeutral = new TH1D("hist_theta_neutral_deg","#theta_{neutral} (^{132}Sn+^{124}Sn @ 20 MeV/u (AMD));#theta (degrees);(1/N_{event})(dN/d#theta)",nbinsx, xlow, xup);

        hist -> GetXaxis() -> CenterTitle();
        hist -> GetYaxis() -> CenterTitle();
        histCharged -> GetXaxis() -> CenterTitle();
        histCharged -> GetYaxis() -> CenterTitle();
        histNeutral -> GetXaxis() -> CenterTitle();
        histNeutral -> GetYaxis() -> CenterTitle();

  labTree -> SetWeight(weight);
  cvs -> cd();
  labTree -> Draw("theta*180/TMath::Pi()>>hist_theta_deg");
  cvsCharged -> cd();
  labTree -> Draw("theta*180/TMath::Pi()>>hist_theta_charged_deg","Z!=0");
  cvsNeutral -> cd();
  labTree -> Draw("theta*180/TMath::Pi()>>hist_theta_neutral_deg","Z==0");

  std::cout << std::endl << "* theta in degrees" << std::endl;
  std::cout << "y-av-0 : " << GetYMean(hist,nbinsx) << std::endl;
  std::cout << "y-av-n : " << GetYMean(histNeutral,nbinsx) << std::endl;
  std::cout << "y-av-c : " << GetYMean(histCharged,nbinsx) << std::endl;
  std::cout << "normal : " << hist -> Integral("width") << std::endl;
  std::cout << "charged: " << histCharged -> Integral("width") << std::endl;
  std::cout << "neutral: " << histNeutral -> Integral("width") << std::endl;
  std::cout << std::endl;

  histData -> cd();
    cvs -> Write();
    cvsCharged -> Write();
    cvsNeutral -> Write();

  cvs -> SaveAs("doc/figures/theta_deg.pdf");
  cvsCharged -> SaveAs("doc/figures/theta_charged_deg.pdf");
  cvsNeutral -> SaveAs("doc/figures/theta_neutral_deg.pdf");

  delete cvs;
  delete cvsCharged;
  delete cvsNeutral;

  delete hist;
  delete histCharged;
  delete histNeutral;
}

void ThetaAnalysis(TTree *labTree, TFile *histData)
{
  int    nbinsx = 60;
  double xlow   = 0;
  double xup    = TMath::Pi();
  double weight = (nbinsx/(xup - xlow))/nEvent;

  TCanvas *cvs = new TCanvas("theta","",800,800);
  TCanvas *cvsCharged = new TCanvas("theta_charged","",800,800);
  TCanvas *cvsNeutral = new TCanvas("theta_neutral","",800,800);

  TCanvas *cvsLog = new TCanvas("theta_log","",800,800);
           cvsLog -> SetLogy(1); 
  TCanvas *cvsChargedLog = new TCanvas("theta_charged_log","",800,800);
           cvsChargedLog -> SetLogy(1); 
  TCanvas *cvsNeutralLog = new TCanvas("theta_neutral_log","",800,800);
           cvsNeutralLog -> SetLogy(1); 

  TCanvas *cvsCharged1 = new TCanvas("theta_charged1","",800,800);
  TCanvas *cvsCharged2 = new TCanvas("theta_charged2","",800,800);
  TCanvas *cvsChargedLog1 = new TCanvas("theta_charged_log1","",800,800);
           cvsChargedLog1 -> SetLogy(1); 
  TCanvas *cvsChargedLog2 = new TCanvas("theta_charged_log2","",800,800);
           cvsChargedLog2 -> SetLogy(1); 

  TCanvas *cvsChargedH1 = new TCanvas("theta_charged_h1","",800,800);
  TCanvas *cvsChargedH2 = new TCanvas("theta_charged_h2","",800,800);
  TCanvas *cvsChargedHLog1 = new TCanvas("theta_charged_h_log1","",800,800);
           cvsChargedHLog1 -> SetLogy(1);
  TCanvas *cvsChargedHLog2 = new TCanvas("theta_charged_h_log2","",800,800);
           cvsChargedHLog2 -> SetLogy(1);

  TH1D *hist = new TH1D("hist_theta","#theta (^{132}Sn+^{124}Sn @ 20 MeV/u (AMD));#theta (radian);(1/N_{event})(dN/d#theta)",nbinsx, xlow, xup);
  TH1D *histCharged = new TH1D("hist_theta_charged","#theta_{charged} (^{132}Sn+^{124}Sn @ 20 MeV/u (AMD));#theta (radian);(1/N_{event})(dN/d#theta)",nbinsx, xlow, xup);
        histCharged -> SetMaximum(12);
  TH1D *histNeutral = new TH1D("hist_theta_neutral","#theta_{neutral} (^{132}Sn+^{124}Sn @ 20 MeV/u (AMD));#theta (radian);(1/N_{event})(dN/d#theta)",nbinsx, xlow, xup);

        hist -> GetXaxis() -> CenterTitle();
        hist -> GetYaxis() -> CenterTitle();
        histCharged -> GetXaxis() -> CenterTitle();
        histCharged -> GetYaxis() -> CenterTitle();
        histNeutral -> GetXaxis() -> CenterTitle();
        histNeutral -> GetYaxis() -> CenterTitle();

  TH1D *histCharged1 = new TH1D("hist_theta_charged1","#theta_{charged}^{A#leq4} (^{132}Sn+^{124}Sn @ 20 MeV/u (AMD));#theta (radian);(1/N_{event})(dN/d#theta)",nbinsx, xlow, xup);
        histCharged1 -> SetMaximum(12);
  TH1D *histCharged2 = new TH1D("hist_theta_charged2","#theta_{charged}^{A>4} (^{132}Sn+^{124}Sn @ 20 MeV/u (AMD));#theta (radian);(1/N_{event})(dN/d#theta)",nbinsx, xlow, xup);
        histCharged2 -> SetMaximum(12);

        histCharged1 -> GetXaxis() -> CenterTitle();
        histCharged1 -> GetYaxis() -> CenterTitle();
        histCharged2 -> GetXaxis() -> CenterTitle();
        histCharged2 -> GetYaxis() -> CenterTitle();

  TH1D *histChargedH1 = new TH1D("hist_theta_chargedH1","#theta_{charged}^{proton} (^{132}Sn+^{124}Sn @ 20 MeV/u (AMD));#theta (radian);(1/N_{event})(dN/d#theta)",nbinsx, xlow, xup);
        histChargedH1 -> SetMaximum(12);
  TH1D *histChargedH2 = new TH1D("hist_theta_chargedH2","#theta_{charged}^{non-proton} (^{132}Sn+^{124}Sn @ 20 MeV/u (AMD));#theta (radian);(1/N_{event})(dN/d#theta)",nbinsx, xlow, xup);
        histChargedH2 -> SetMaximum(12);

        histChargedH1 -> GetXaxis() -> CenterTitle();
        histChargedH1 -> GetYaxis() -> CenterTitle();
        histChargedH2 -> GetXaxis() -> CenterTitle();
        histChargedH2 -> GetYaxis() -> CenterTitle();

  labTree -> SetWeight(weight);
  labTree -> Draw("theta>>hist_theta");
  labTree -> Draw("theta>>hist_theta_charged","Z!=0");
  labTree -> Draw("theta>>hist_theta_charged1","Z!=0&&A<=4");
  labTree -> Draw("theta>>hist_theta_charged2","Z!=0&&A>4");
  labTree -> Draw("theta>>hist_theta_chargedH1","Z!=0&&A==1");
  labTree -> Draw("theta>>hist_theta_chargedH2","Z!=0&&A!=1");
  labTree -> Draw("theta>>hist_theta_neutral","Z==0");

  std::cout << std::endl << "* theta in radian" << std::endl;
  std::cout << "y-av-0 : " << GetYMean(hist,nbinsx) << std::endl;
  std::cout << "y-av-n : " << GetYMean(histNeutral,nbinsx) << std::endl;
  std::cout << "y-av-c : " << GetYMean(histCharged,nbinsx) << std::endl;
  std::cout << "normal : " << hist -> Integral("width") << std::endl;
  std::cout << "charged: " << histCharged -> Integral("width") << std::endl;
  std::cout << "neutral: " << histNeutral -> Integral("width") << std::endl;
  std::cout << std::endl;

  cvs -> cd();
  hist -> Draw();
  //hist -> Fit("f_theta","");
  //f -> Draw("same");

  cvsLog -> cd();
  hist -> Draw();

  cvsCharged -> cd();
  histCharged -> Draw();
  //histCharged -> Fit("f_theta_charged","");
  //fCharged -> Draw("same");

  cvsChargedLog -> cd();
  histCharged -> Draw();

  cvsCharged1 -> cd();
  histCharged1 -> Draw();
  cvsCharged2 -> cd();
  histCharged2 -> Draw();
  cvsChargedLog1 -> cd();
  histCharged1 -> Draw();
  cvsChargedLog2 -> cd();
  histCharged2 -> Draw();

  cvsChargedH1 -> cd();
  histChargedH1 -> Draw();
  cvsChargedH2 -> cd();
  histChargedH2 -> Draw();
  cvsChargedHLog1 -> cd();
  histChargedH1 -> Draw();
  cvsChargedHLog2 -> cd();
  histChargedH2 -> Draw();

  cvsNeutral -> cd();
  histNeutral -> Draw();
  //histNeutral -> Fit("f_theta_neutral","");
  //fNeutral -> Draw("same");

  cvsNeutralLog -> cd();
  histNeutral -> Draw();

  histData -> cd();
  cvs -> Write();
  cvsCharged -> Write();
  cvsNeutral -> Write();
  cvsLog -> Write();
  cvsChargedLog -> Write();
  cvsNeutralLog -> Write();

  cvs -> SaveAs("doc/figures/theta.pdf");
  cvsCharged -> SaveAs("doc/figures/theta_charged.pdf");
  cvsCharged1 -> SaveAs("doc/figures/theta_charged1.pdf");
  cvsCharged2 -> SaveAs("doc/figures/theta_charged2.pdf");
  cvsChargedH1 -> SaveAs("doc/figures/theta_chargedH1.pdf");
  cvsChargedH2 -> SaveAs("doc/figures/theta_chargedH2.pdf");
  cvsNeutral -> SaveAs("doc/figures/theta_neutral.pdf");
  cvsLog -> SaveAs("doc/figures/theta_log.pdf");
  cvsChargedLog1 -> SaveAs("doc/figures/theta_charged_log1.pdf");
  cvsChargedLog2 -> SaveAs("doc/figures/theta_charged_log2.pdf");
  cvsChargedHLog1 -> SaveAs("doc/figures/theta_chargedH_log1.pdf");
  cvsChargedHLog2 -> SaveAs("doc/figures/theta_chargedH_log2.pdf");
  cvsChargedLog -> SaveAs("doc/figures/theta_charged_log.pdf");
  cvsNeutralLog -> SaveAs("doc/figures/theta_neutral_log.pdf");

  delete cvs;
  delete cvsCharged;
  delete cvsCharged1;
  delete cvsCharged2;
  delete cvsChargedH1;
  delete cvsChargedH2;
  delete cvsNeutral;
  delete cvsLog;
  delete cvsChargedLog;
  delete cvsChargedLog1;
  delete cvsChargedLog2;
  delete cvsChargedHLog1;
  delete cvsChargedHLog2;
  delete cvsNeutralLog;

  delete hist;
  delete histCharged;
  delete histCharged1;
  delete histCharged2;
  delete histChargedH1;
  delete histChargedH2;
  delete histNeutral;
}

void PhiAnalysis(TTree *labTree, TFile *histData)
{
  int    nbinsx = 50;
  double xlow   = 0;
  double xup    = 2*TMath::Pi();
  double weight = (nbinsx/(xup - xlow))/nEvent;

  TCanvas *cvs = new TCanvas("phi","",800,800);
  TCanvas *cvsCharged = new TCanvas("phi_charged","",800,800);
           //cvsCharged -> SetLeftMargin(2.0);
  TCanvas *cvsNeutral = new TCanvas("phi_neutral","",800,800);
  TH1D *hist = new TH1D("hist_phi","#phi (^{132}Sn+^{124}Sn @ 20 MeV/u (AMD));#phi (radian);(1/N_{event})(dN/d#phi)",nbinsx, xlow, xup);
        hist -> SetMinimum(0);
  TH1D *histCharged = new TH1D("hist_phi_charged","#phi_{charged} (^{132}Sn+^{124}Sn @ 20 MeV/u (AMD));#phi (radian);(1/N_{event})(dN/d#phi)",nbinsx, xlow, xup);
        histCharged -> SetMinimum(0);
  TH1D *histNeutral = new TH1D("hist_phi_neutral","#phi_{neutral} (^{132}Sn+^{124}Sn @ 20 MeV/u (AMD));#phi (radian);(1/N_{event})(dN/d#phi)",nbinsx, xlow, xup);
        histNeutral -> SetMinimum(0);

        hist -> GetXaxis() -> CenterTitle();
        hist -> GetYaxis() -> CenterTitle();
        histCharged -> GetXaxis() -> CenterTitle();
        histCharged -> GetYaxis() -> CenterTitle();
        histNeutral -> GetXaxis() -> CenterTitle();
        histNeutral -> GetYaxis() -> CenterTitle();

  labTree -> SetWeight(weight);
  labTree -> Draw("phi>>hist_phi");
  labTree -> Draw("phi>>hist_phi_charged","Z!=0");
  labTree -> Draw("phi>>hist_phi_neutral","Z==0");

  std::cout << std::endl << "* phi in radian" << std::endl;
  std::cout << "y-av-0 : " << GetYMean(hist,nbinsx) << std::endl;
  std::cout << "y-av-n : " << GetYMean(histNeutral,nbinsx) << std::endl;
  std::cout << "y-av-c : " << GetYMean(histCharged,nbinsx) << std::endl;
  std::cout << "normal : " << hist -> Integral("width") << std::endl;
  std::cout << "charged: " << histCharged -> Integral("width") << std::endl;
  std::cout << "neutral: " << histNeutral -> Integral("width") << std::endl;
  std::cout << std::endl;

  cvs -> cd();
  hist -> Draw();

  cvsCharged -> cd();
  histCharged -> Draw();

  cvsNeutral -> cd();
  histNeutral -> Draw();

  histData -> cd();
  cvs -> Write();
  cvsCharged -> Write();
  cvsNeutral -> Write();

  cvs -> SaveAs("doc/figures/phi.pdf");
  cvsCharged -> SaveAs("doc/figures/phi_charged.pdf");
  cvsNeutral -> SaveAs("doc/figures/phi_neutral.pdf");

  delete cvs;
  delete cvsCharged;
  delete cvsNeutral;
  delete hist;
  delete histCharged;
  delete histNeutral;
}

void EKinAnalysis(TTree *labTree, TFile *histData)
{
  int    nbinsx = 100;
  double xlow   = 0;
  double xup    = 1140;
  double weight = (nbinsx/(xup - xlow))/nEvent;

  TCanvas *cvs = new TCanvas("eKin","",800,800);
           cvs -> SetLogy(1);
  TCanvas *cvsCharged = new TCanvas("eKin_charged","",800,800);
           cvsCharged -> SetLogy(1);
  TCanvas *cvsNeutral = new TCanvas("eKin_neutral","",800,800);
  TH1D *hist = new TH1D("hist_eKin","E_{kin} (^{132}Sn+^{124}Sn @ 20 MeV/u (AMD));E_{kin} (MeV);(1/N_{event})(dN/dE_{kin})",nbinsx, xlow, xup);
  TH1D *histCharged = new TH1D("hist_eKin_charged","E_{kin}^{charged} (^{132}Sn+^{124}Sn @ 20 MeV/u (AMD));E_{kin} (MeV);(1/N_{event})(dN/dE_{kin})",nbinsx, xlow, xup);
  TH1D *histNeutral = new TH1D("hist_eKin_neutral","E_{kin}^{neutral} (^{132}Sn+^{124}Sn @ 20 MeV/u (AMD));E_{kin} (MeV);(1/N_{event})(dN/dE_{kin})",nbinsx, xlow, 90);

        hist -> GetXaxis() -> CenterTitle();
        hist -> GetYaxis() -> CenterTitle();
        histCharged -> GetXaxis() -> CenterTitle();
        histCharged -> GetYaxis() -> CenterTitle();
        histNeutral -> GetXaxis() -> CenterTitle();
        histNeutral -> GetYaxis() -> CenterTitle();


  labTree -> SetWeight(weight);
  labTree -> Draw("eKin>>hist_eKin");
  labTree -> Draw("eKin>>hist_eKin_charged","Z!=0");

  weight = (nbinsx/(90 - xlow))/nEvent;
  labTree -> SetWeight(weight);
  labTree -> Draw("eKin>>hist_eKin_neutral","Z==0");

  std::cout << std::endl << "* kinetic energy" << std::endl;
  std::cout << "y-av-0 : " << GetYMean(hist,nbinsx) << std::endl;
  std::cout << "y-av-n : " << GetYMean(histNeutral,nbinsx) << std::endl;
  std::cout << "y-av-c : " << GetYMean(histCharged,nbinsx) << std::endl;
  std::cout << "normal : " << hist -> Integral("width") << std::endl;
  std::cout << "charged: " << histCharged -> Integral("width") << std::endl;
  std::cout << "neutral: " << histNeutral -> Integral("width") << std::endl;
  std::cout << std::endl;

  cvs -> cd();
  hist -> Draw();

  cvsCharged -> cd();
  histCharged -> Draw();

  cvsNeutral -> cd();
  histNeutral -> Draw();

  histData -> cd();
  cvs -> Write();
  cvsCharged -> Write();
  cvsNeutral -> Write();

  cvs -> SaveAs("doc/figures/eKin.pdf");
  cvsCharged -> SaveAs("doc/figures/eKin_charged.pdf");
  cvsNeutral -> SaveAs("doc/figures/eKin_neutral.pdf");

  delete cvs;
  delete cvsCharged;
  delete cvsNeutral;
  delete hist;
  delete histCharged;
  delete histNeutral;
}

void PTAnalysis(TTree *labTree, TFile *histData)
{
  int    nbinsx = 100;
  double xlow   = 0;
  double xup    = 4300;
  double weight = (nbinsx/(xup - xlow))/nEvent;

  TCanvas *cvs = new TCanvas("pT","",800,800);
           cvs -> SetLogy(1);
  TCanvas *cvsCharged = new TCanvas("pT_charged","",800,800);
           cvsCharged -> SetLogy(1);
  TCanvas *cvsNeutral = new TCanvas("pT_neutral","",800,800);
           //cvsNeutral -> SetLogy(1);
  TH1D *hist = new TH1D("hist_pT","p_{T} (^{132}Sn+^{124}Sn @ 20 MeV/u (AMD));p_{T} (MeV/c);(1/N_{event})(dN/dp_{T})",nbinsx, xlow, xup);
  TH1D *histCharged = new TH1D("hist_pT_charged","p_{T}^{charged} (^{132}Sn+^{124}Sn @ 20 MeV/u (AMD));p_{T} (MeV/c);(1/N_{event})(dN/dp_{T})",nbinsx, xlow, xup);
  TH1D *histNeutral = new TH1D("hist_pT_neutral","p_{T}^{neutral} (^{132}Sn+^{124}Sn @ 20 MeV/u (AMD));p_{T} (MeV/c);(1/N_{event})(dN/dp_{T})",nbinsx, xlow, 350);
        //histNeutral -> GetYaxis() -> SetTitleOffset(1.6);

        hist -> GetXaxis() -> CenterTitle();
        hist -> GetYaxis() -> CenterTitle();
        histCharged -> GetXaxis() -> CenterTitle();
        histCharged -> GetYaxis() -> CenterTitle();
        histNeutral -> GetXaxis() -> CenterTitle();
        histNeutral -> GetYaxis() -> CenterTitle();

  labTree -> SetWeight(weight);
  labTree -> Draw("pT>>hist_pT");
  labTree -> Draw("pT>>hist_pT_charged","Z!=0");

  weight = (nbinsx/(350 - xlow))/nEvent;
  labTree -> SetWeight(weight);
  labTree -> Draw("pT>>hist_pT_neutral","Z==0");

  std::cout << std::endl << "* momentum" << std::endl;
  std::cout << "y-av-0 : " << GetYMean(hist,nbinsx) << std::endl;
  std::cout << "y-av-n : " << GetYMean(histNeutral,nbinsx) << std::endl;
  std::cout << "y-av-c : " << GetYMean(histCharged,nbinsx) << std::endl;
  std::cout << "normal : " << hist -> Integral("width") << std::endl;
  std::cout << "charged: " << histCharged -> Integral("width") << std::endl;
  std::cout << "neutral: " << histNeutral -> Integral("width") << std::endl;
  std::cout << std::endl;

  cvs -> cd();
  hist -> Draw();

  cvsCharged -> cd();
  histCharged -> Draw();

  cvsNeutral -> cd();
  histNeutral -> Draw();

  histData -> cd();
  cvs -> Write();
  cvsCharged -> Write();
  cvsNeutral -> Write();

  cvs -> SaveAs("doc/figures/pT.pdf");
  cvsCharged -> SaveAs("doc/figures/pT_charged.pdf");
  cvsNeutral -> SaveAs("doc/figures/pT_neutral.pdf");

  delete cvs;
  delete cvsCharged;
  delete cvsNeutral;
  delete hist;
  delete histCharged;
  delete histNeutral;
}

void RapidityAnalysis(TTree *labTree, TFile *histData)
{
  int    nbinsx = 100;
  double xlow   = -1.2;
  double xup    = 2.2;
  double weight = (nbinsx/(xup - xlow))/nEvent;

  TCanvas *cvs = new TCanvas("y","",800,800);
  TCanvas *cvsCharged = new TCanvas("y_charged","",800,800);
  TCanvas *cvsNeutral = new TCanvas("y_neutral","",800,800);

  TCanvas *cvsCharged1 = new TCanvas("y_charged1","",800,800);
  TCanvas *cvsCharged2 = new TCanvas("y_charged2","",800,800);
  TCanvas *cvsChargedH1 = new TCanvas("y_chargedH1","",800,800);
  TCanvas *cvsChargedH2 = new TCanvas("y_chargedH2","",800,800);

  TH1D *hist = new TH1D("hist_y","y_{norm} (^{132}Sn+^{124}Sn @ 20 MeV/u (AMD));y_{norm};(1/N_{event})(dN/dy_{norm})",nbinsx, xlow, xup);
  TH1D *histCharged = new TH1D("hist_y_charged","y_{norm}^{charged} (^{132}Sn+^{124}Sn @ 20 MeV/u (AMD));y_{norm};(1/N_{event})(dN/dy_{norm})",nbinsx, xlow, xup);
        histCharged -> SetMaximum(15);
  TH1D *histNeutral = new TH1D("hist_y_neutral","y_{norm}^{neutral} (^{132}Sn+^{124}Sn @ 20 MeV/u (AMD));y_{norm};(1/N_{event})(dN/dy_{norm})",nbinsx, xlow, xup);

        hist -> GetXaxis() -> CenterTitle();
        hist -> GetYaxis() -> CenterTitle();
        histCharged -> GetXaxis() -> CenterTitle();
        histCharged -> GetYaxis() -> CenterTitle();
        histNeutral -> GetXaxis() -> CenterTitle();
        histNeutral -> GetYaxis() -> CenterTitle();

  TH1D *histCharged1 = new TH1D("hist_y_charged1","y_{charged}^{A#leq4} (^{132}Sn+^{124}Sn @ 20 MeV/u (AMD));y_{norm};(1/N_{event})(dN/dy_{norm})",nbinsx, xlow, xup);
        histCharged1 -> SetMaximum(15);
  TH1D *histCharged2 = new TH1D("hist_y_charged2","y_{charged}^{A>4} (^{132}Sn+^{124}Sn @ 20 MeV/u (AMD));y_{norm};(1/N_{event})(dN/dy_{norm})",nbinsx, xlow, xup);
        histCharged2 -> SetMaximum(15);

        histCharged1 -> GetXaxis() -> CenterTitle();
        histCharged1 -> GetYaxis() -> CenterTitle();
        histCharged2 -> GetXaxis() -> CenterTitle();
        histCharged2 -> GetYaxis() -> CenterTitle();

  TH1D *histChargedH1 = new TH1D("hist_y_chargedH1","y_{charged}^{proton} (^{132}Sn+^{124}Sn @ 20 MeV/u (AMD));y_{norm};(1/N_{event})(dN/dy_{norm})",nbinsx, xlow, xup);
        histChargedH1 -> SetMaximum(15);
  TH1D *histChargedH2 = new TH1D("hist_y_chargedH2","y_{charged}^{non-proton} (^{132}Sn+^{124}Sn @ 20 MeV/u (AMD));y_{norm};(1/N_{event})(dN/dy_{norm})",nbinsx, xlow, xup);
        histChargedH2 -> SetMaximum(15);

        histChargedH1 -> GetXaxis() -> CenterTitle();
        histChargedH1 -> GetYaxis() -> CenterTitle();
        histChargedH2 -> GetXaxis() -> CenterTitle();
        histChargedH2 -> GetYaxis() -> CenterTitle();

  labTree -> SetWeight(weight);

  cvs -> cd();
  labTree -> Draw("rapidity>>hist_y");

  cvsCharged -> cd();
  labTree -> Draw("rapidity>>hist_y_charged","Z!=0");

  cvsCharged1 -> cd();
  labTree -> Draw("rapidity>>hist_y_charged1","Z!=0&&A<=4");

  cvsCharged2 -> cd();
  labTree -> Draw("rapidity>>hist_y_charged2","Z!=0&&A>4");

  cvsChargedH1 -> cd();
  labTree -> Draw("rapidity>>hist_y_chargedH1","Z!=0&&A==1");

  cvsChargedH2 -> cd();
  labTree -> Draw("rapidity>>hist_y_chargedH2","Z!=0&&A!=1");

  cvsNeutral -> cd();
  labTree -> Draw("rapidity>>hist_y_neutral","Z==0");

  std::cout << std::endl << "* rapidity" << std::endl;
  std::cout << "y-av-0 : " << GetYMean(hist,nbinsx) << std::endl;
  std::cout << "y-av-n : " << GetYMean(histNeutral,nbinsx) << std::endl;
  std::cout << "y-av-c : " << GetYMean(histCharged,nbinsx) << std::endl;
  std::cout << "normal : " << hist -> Integral("width") << std::endl;
  std::cout << "charged: " << histCharged -> Integral("width") << std::endl;
  std::cout << "neutral: " << histNeutral -> Integral("width") << std::endl;
  std::cout << std::endl;

  histData -> cd();
  cvs -> Write();
  cvsCharged -> Write();
  cvsNeutral -> Write();

  cvs -> SaveAs("doc/figures/y.pdf");
  cvsCharged -> SaveAs("doc/figures/y_charged.pdf");
  cvsCharged1 -> SaveAs("doc/figures/y_charged1.pdf");
  cvsCharged2 -> SaveAs("doc/figures/y_charged2.pdf");
  cvsChargedH1 -> SaveAs("doc/figures/y_chargedH1.pdf");
  cvsChargedH2 -> SaveAs("doc/figures/y_chargedH2.pdf");
  cvsNeutral -> SaveAs("doc/figures/y_neutral.pdf");

  delete cvs;
  delete cvsCharged;
  delete cvsNeutral;
  delete hist;
  delete histCharged;
  delete histNeutral;
}

double GetYMean(TH1D *hist, int nbinsx)
{
  double meanContent=0;
  for(int i=0; i<nbinsx; i++) meanContent += hist -> GetBinContent(i+1);
  meanContent /= nbinsx;

  return meanContent;
}
